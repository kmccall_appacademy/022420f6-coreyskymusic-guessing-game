# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.
 def guessing_game
   winning_number = (1..100).to_a.shuffle.last
   guess = nil
   num_guesses = 0
   until guess == winning_number
     puts "Guess a number between 1 and 100"
     guess = gets.chomp.to_i
     num_guesses += 1
     if guess > winning_number
       puts "Your guess #{guess} was too high"
     else
       puts "Your guess #{guess} was too low"
     end
   end

   puts "Congratulations you guessed #{winning_number} the winning number"
   puts "You did it in #{num_guesses} guesses."
 end

def file_shuffler
  puts "Please enter a file name:"
  file = gets.strip
  contents = File.readlines(file)
  shuffled_contents = contents.dup.shuffle!

  File.open("#{file}--shuffle.txt", "w") do |file|
    shuffled_contents.each do |line|
      file.puts line
    end
  end
end


if __FILE__ == $PROGRAM_NAME
  file_shuffler
end

# def shuffle_file(filename)
#   base = File.basename(filename, ".*")
#   extension = File.extname(filename)
#   File.open("#{base}-shuffled#{extension}", "w") do |f|
#     File.readlines(filename).shuffle.each do |line|
#       f.puts line.chomp
#     end
#   end
# end
#
# if __FILE__ == $PROGRAM_NAME
#   if ARGV.length == 1
#     shuffle_file(ARGV.shift)
#   else
#     puts "ENTER FILENAME TO SHUFFLE:"
#     filename = gets.chomp
#     shuffle_file(filename)
#   end
# end
